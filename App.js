import 'react-native-gesture-handler';
import React from 'react';
import {
  TouchableOpacity,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import FirstScreen from './src/screen/screen1/index';
import SecondScreen from './src/screen/screen2/index';
import ThirdScreen from './src/screen/screen3/index';
import FourthScreen from './src/screen/screen4/index';
import MapView from './src/screen/screen3/mapsView';

const stack = createStackNavigator();

const Screen3 = () => {
  return(
    <stack.Navigator>
      <stack.Screen name="ThirdSCreen" component={ThirdScreen} options={{headerShown:false}}/>
      <stack.Screen name="MapsView" component={MapView} options={{headerShown:false}}/>
    </stack.Navigator>
  )
}

const App =()=>{
  return(
  <NavigationContainer>
    <stack.Navigator>
      <stack.Screen name="FirstScreen" component={FirstScreen} options={{headerShown:false}}/>
      <stack.Screen name="SecondScreen" component={SecondScreen} options={{headerShown:false}}/>
      <stack.Screen name="ThirdScreen" component={Screen3} options={{headerShown:false}} />
      <stack.Screen name="FourthScreen" component={FourthScreen} options={{headerShown:false}}/>
    </stack.Navigator>
  </NavigationContainer>
  )
}
const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;