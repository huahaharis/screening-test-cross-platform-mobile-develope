import Types from '../../../utils/Types';

export const inputName = (name)=>{
    console.log(name);
    return(dispatch)=>{
        dispatch(inputNameSuccess(name))
    }
}

const inputNameSuccess =(name)=>{
    return {
        type: Types.INPUT_NAME,
        name
      }
}