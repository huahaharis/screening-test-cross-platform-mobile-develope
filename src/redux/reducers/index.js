import {combineReducers} from 'redux';
import InputNameReducer from './reducerScreen1/inputNameReducers';
import PickDataReducer from './reducerScreen3/selectDataReducers'

export default combineReducers({
    InputNameReducer,
    PickDataReducer
})