import Types from '../../../utils/Types';

const INITIAL_STATE = {
  nameInput: null
}

const inputName = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case Types.INPUT_NAME:
        state = {
          ...state,
          nameInput: action.name
        }
        break
      default:
        break
    }
  
    return state
  }

  export default inputName