import React from 'react';
import {
  View, 
  Text, 
  StyleSheet, 
  TextInput, 
  TouchableOpacity, 
  ImageBackground,
  Dimensions,
  Image,
  StatusBar,
  ToastAndroid
} from 'react-native';
import * as actions from '../../redux/actions/index';
import {useDispatch, useSelector} from 'react-redux';
import { useNavigation } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const FirstScreen = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const [name, setName]= React.useState('')
  const [palindrome, setPlaindrome] = React.useState('')
  const Name = useSelector(state => state)
  console.log(Name);

  const inputName = (name) => {
    dispatch(actions.inputName(name))
    navigation.navigate('SecondScreen')
  }


function fastestIsPalindrome(str) {
  var str = str.replace(/[^a-zA-Z0-9]+/gi, '').toLowerCase();
  if(str == str.split('').reverse().join('') === true){
    ToastAndroid.showWithGravityAndOffset(
      "it's palindrome",
      ToastAndroid.LONG,
      ToastAndroid.CENTER,
      25,
      50
    );
  } else {
    ToastAndroid.showWithGravityAndOffset(
      "not palindrome",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
      25,
      50
    );
  }
}

  return(
    <View style={style.container}>
      <StatusBar hidden/>
      <ImageBackground 
      source={require('../../components/assets/bg_bright.png')}
      style={{width: windowWidth, height: "60%"}}>
        <View style={{justifyContent: 'center', alignItems: 'center', marginTop: "25%"}}>
        <Text style={{color: 'black', fontSize: 20}}>
        Welcome
        </Text>
        <Text style={{color: 'black', marginTop: '3%'}}>
          This is app for suitmedia mobile developer test
        </Text>
        </View>
        </ImageBackground>
        <View style={style.halfUp}>
        <Image 
        source={require('../../components/assets/img_avatar.png')}
        style={{height: '30%', width: '36%' , marginBottom: 30}}/>
        <TextInput
              style={style.inputField}
              onChangeText={text => setName(text)}
              value={name}
              editable={true}
              maxLength={40}
              multiline={false}
              placeholder="Type name here"
            />
          <TextInput
              style={style.inputField2}
              onChangeText={text => setPlaindrome(text)}
              value={palindrome}
              editable={true}
              maxLength={40}
              multiline={false}
              placeholder="type text palindrome"
            />
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity 
            style={style.button}
            onPress={()=> inputName(name)}>
              <Text style={{ 
                justifyContent:'center',
                alignItems:'center', 
                marginHorizontal: '28%', 
                marginVertical: 1}}>
              Next
              </Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={style.button2}
            onPress={()=> fastestIsPalindrome(palindrome)}>
              <Text style={{ 
                justifyContent:'center',
                alignItems:'center', 
                marginHorizontal: '16.5%', 
                marginVertical: '3%',
                marginBottom: '8%'
                }}>
              Check
              </Text>
          </TouchableOpacity>
         </View>
       </View>
    </View>
  )
}
const style = StyleSheet.create({
  container:{
    flex:1
  },
  halfUp:{
    backgroundColor: '#F8F5F3', 
    elevation: 10,
    shadowOpacity: 4,
    width: windowWidth /1.5, 
   height: '50%',
    borderRadius: 15/2, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginHorizontal: '16.2%', 
    marginTop: '-49%'
  },
  inputField: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
  },
  inputField2: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginTop: '3%'
  },
  button: {
    width: '42%',
    height: '65%',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: -5,
    marginTop: 5,
    marginBottom: 5,
  },
  button2: {
    width: '32%',
    height: '65%',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
})

export default FirstScreen