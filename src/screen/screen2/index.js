import React from 'react';
import {
  View, 
  Text, 
  StyleSheet, 
  TextInput, 
  TouchableOpacity, 
  ImageBackground,
  Dimensions,
  Image,
  StatusBar
} from 'react-native';
import * as actions from '../../redux/actions/index';
import {useDispatch, useSelector} from 'react-redux';
import { useNavigation } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const SecondScreen = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const Name = useSelector(state => state.InputNameReducer.nameInput)
  const [name, setName]= React.useState('')
  const data = useSelector(state => state.PickDataReducer.data.name)


  const inputName = name => dispatch(inputName(name))

  return(
    <View style={style.container}>
      <StatusBar hidden/>
      <ImageBackground 
      source={require('../../components/assets/bg_bright.png')}
      style={{width: windowWidth, height: "60%"}}>
        <View style={{marginTop: "15%"}}>
        <View style={{height: '1.9%', width: '70%', backgroundColor: '#F99C5E', borderRadius: 60, marginHorizontal: '30%', top: '38%'}}/>
        <View style={{height: '1.9%', width: '90%', backgroundColor: '#F99C5E', borderRadius: 60, marginHorizontal: '27%', marginVertical: '2%', top: '38%'}}/>
        <View style={{height: '1.9%', width: '70%', backgroundColor: '#F99C5E', borderRadius: 60, marginHorizontal: '30%', top: '38%'}}/>
        <Text style={{color: 'black', fontSize: 26}}>
        Hallo, 
        </Text>
        <Text style={{color: '#F99C5E',fontSize: 26, fontWeight: 'bold'}}>
          {Name}
        </Text>
        </View>
        </ImageBackground>
        <TouchableOpacity 
        style={style.button}
        onPress={()=> navigation.navigate('FourthScreen')}>
          <Text style={{ 
          marginHorizontal: '10%', 
          marginVertical: '1%'}}>
          Choose Guest
          </Text>
        </TouchableOpacity>
        <TouchableOpacity 
        style={style.button2}
        onPress={()=> navigation.navigate('ThirdScreen')}>
          {data === undefined ? (
          <Text style={{ 
          marginHorizontal: '10%', 
          marginVertical: 0}}>
          Choose Event
          </Text>
          ):(
          <Text style={{ 
            marginHorizontal: '10%', 
            marginVertical: 0}}>
            {data}
            </Text>
            )}
        </TouchableOpacity>
        <Image 
            source={require('../../components/assets/img_suitmedia.png')}
            style={{
              height: '35%', 
              width: '80%', 
              marginHorizontal: '10.1%',
              marginVertical: '12%'}}/>
          <View style={{marginVertical: '-5%', height: '10%'}}>
            <Image 
            source={require('../../components/assets/img_bg_bottom.png')}
            style={{height: '80%', width: windowWidth}}/>
          </View>
    </View>
  )
}
const style = StyleSheet.create({
  container:{
    flex:1
  },
  halfUp:{
    backgroundColor: '#F8F5F3', 
    elevation: 10,
    shadowOpacity: 4,
    width: windowWidth /1.5, 
   height: '50%',
    borderRadius: 15/2, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginHorizontal: '16.2%', 
    marginTop: '-49%'
  },
  button: {
    width: '62%',
    height: '8%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '18%',
    marginVertical: '-35%',
    marginTop: '-30%'
  },
  button2: {
    width: '62%',
    height: '8%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '18%',
    marginVertical: '5%',
    marginTop: '8%'
  },
})

export default SecondScreen