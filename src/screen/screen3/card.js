import React from 'react';
import {View, Text, Image, StatusBar, StyleSheet, Dimensions, TouchableOpacity} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Card, CardItem } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../../redux/actions/index';

const Data =[
    {
        'id':'1',
        'name':'didit',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/2.jpeg')
    },
    {
        'id':'2',
        'name':'andi',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/1.jpeg')
    },
    {
        'id':'3',
        'name':'agus',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/1.jpeg')
    },
    {
        'id':'4',
        'name':'budi',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/2.jpeg')
    }
]

const Cards=(props)=>{
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const getData=(id, name)=>{
      dispatch(actions.selectData(id, name))
      navigation.navigate('SecondScreen')
    }
    return(
        // Data.map((res, index)=>{
    <Card>
            <CardItem button onPress={()=>getData(props.id, props.nama)}>
              <Left>
            <Image  
              source={props.Image}
              style={{height: 100, width: 150}}
            />
              </Left>
              <Body>
                <Text>
                Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting.
                </Text>
              </Body>
            </CardItem>
          </Card>
        //    })
    )
}
export default Cards