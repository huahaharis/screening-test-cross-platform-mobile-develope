import React from 'react';
import {View, Text, Image, StatusBar, StyleSheet, Dimensions, TouchableOpacity} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Card, CardItem } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import Cards from './card';
import * as actions from '../../redux/actions/index';
const windowWidth = Dimensions.get('window').width;

const Data =[
  {
      'id':'1',
      'name':'didit',
      'date':'Apr, 3, 2020',
      'image':require('../../components/assets/2.jpeg')
  },
  {
      'id':'2',
      'name':'andi',
      'date':'Apr, 3, 2020',
      'image':require('../../components/assets/1.jpeg')
  },
  {
      'id':'3',
      'name':'agus',
      'date':'Apr, 3, 2020',
      'image':require('../../components/assets/1.jpeg')
  },
  {
      'id':'4',
      'name':'budi',
      'date':'Apr, 3, 2020',
      'image':require('../../components/assets/2.jpeg')
  }
]


const ThirdScreen = () => {
  const navigation = useNavigation()
  const [id, setId]=React.useState('')
  const [nama, setName]=React.useState('')
  const dispatch = useDispatch()

  const getData=(id, name)=>{
    dispatch(actions.selectData(id, name))
    navigation.navigate('SecondScreen')
  }

  return(
    <View>
      <StatusBar hidden/>
        <Header style={{backgroundColor: '#F99C5E'}}>
          <Left>
            <Button transparent onPress={()=>navigation.goBack()} >
              <Image 
              source={require('../../components/assets/ic_back_white.png')} 
              style={{height: 13, width: 10}}/>
            </Button>
          </Left>
          <Body>
            <Title>EVENTS</Title>
          </Body>
          <Right>
            <Button transparent>
            <Image 
              source={require('../../components/assets/ic_search_white.png')} 
              style={{height: 17, width: 15}}
              />
            </Button>
            <Button transparent onPress={()=> navigation.navigate('MapsView')}>
              <Image 
              source={require('../../components/assets/ic_map_view.png')} 
              style={{height: 17, width: 15}}
              />
            </Button>
          </Right>
        </Header>
        {Data.map((res, index)=>(
        <Cards 
        Image={require('../../components/assets/1.jpeg')}
        id={res.id}
        nama={res.name}
        key={index}
        Image={res.image}
      />
      ))}
    </View>
  )
}

const style = StyleSheet.create({
  container:{
    flex:1
  },
  halfUp:{
    backgroundColor: 'red', 
    elevation: 10,
    shadowOpacity: 4,
    width: '30%', 
   height: '50%',
    borderRadius: 15/2, 
    justifyContent: 'center', 
    alignItems: 'center',
    marginHorizontal: '16.2%', 
    marginTop: '-49%'
  },
  inputField: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    // justifyContent: 'center',
    // alignItems: 'center',
    // alignContent: 'center'
  },
  button: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
})

export default ThirdScreen