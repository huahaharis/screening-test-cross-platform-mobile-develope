import React from 'react';
import MapsView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import {View, Text, StyleSheet, PermissionsAndroid, ScrollView, Image} from 'react-native';
import { Left, Body, Card, CardItem } from 'native-base';

let API_KEY = "AIzaSyDobzrzLlTIZcQlpK0bdbzG0VEbqSdO3Xs"
const Data =[
    {
        'id':'1',
        'name':'didit',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/2.jpeg'),
        'latitude': 37.419983,
        'longitude': -122.044
    },
    {
        'id':'2',
        'name':'andi',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/1.jpeg'),
        'latitude': 37.3219983,
        'longitude': -122.094
    },
    {
        'id':'3',
        'name':'agus',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/1.jpeg'),
        'latitude': 37.5219983,
        'longitude': -120.084
    },
    {
        'id':'4',
        'name':'budi',
        'date':'Apr, 3, 2020',
        'image':require('../../components/assets/2.jpeg'),
        'latitude': 37.4619983,
        'longitude': -122.184
    }
]

const MapView = () => {
    const [latitudeData, setLatitudeData]=React.useState(null)
    const [longitudeData, setLongitudeData]=React.useState(null)

    const getData = (lat, long) => {
        setLatitudeData(lat)
        setLongitudeData(long)
        console.log(lat, long);

    }

    return(
     <View style={StyleSheet.absoluteFillObject}>
      <MapsView
      provider={PROVIDER_GOOGLE}
      style={StyleSheet.absoluteFillObject}
      key={API_KEY}
      initialRegion={{
      latitude: 37.4219983,
      longitude: -122.084,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
      }}
      >
          <Marker 
            coordinate={{latitude: 7.4219983, longitude: -122.084}}
            pinColor={"purple"}
            title={"title"}
            description={"description"}
          />
    </MapsView>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{height: 50, width: undefined}}> 
      <View style={{flexDirection:'row', alignItems: 'baseline', justifyContent:'center'}}>
          {Data.map((res, index)=>(
         <Card key={index} style={{borderRadius: 40}}>
            <CardItem button onPress={()=>getData(res.latitude, res.longitude)} style={{borderRadius: 30}}>
              <Left>
            <Image  
              source={res.image}
              style={{height: 80, width: 100}}
            />
              </Left>
              <Body>
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                Lorem Ipsum adalah 
                </Text>
              </Body>
            </CardItem>
          </Card>
          ))}
      </View>
        </ScrollView>
    </View>
    )
}

export default MapView