import React from 'react';
import {View, Text, Image, StatusBar, StyleSheet, FlatList, RefreshControl, ToastAndroid} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import Profile from './profile';
import axios from 'axios';
import {setKey,getKey} from '../../utils/shared-pref';


const FourthScreen = () => {
      const navigation = useNavigation()
      const [data, setData]=React.useState([])
      const [page, setPage]=React.useState(1)
      const [refreshing, setRefreshing] = React.useState(false);
      const [loadingMore, setLoadingMore] = React.useState(false);


  React.useEffect(async()=>{
    const cacheData = await getKey('data');
    axios.get(`https://reqres.in/api/users?page=${page}`)
        .then((resp)=>{
          console.log(resp.data.data);
          setData(resp.data.data.concat(data))
          setKey('data', JSON.stringify(data))
          setRefreshing(false)
        })
        .catch((err)=>{
        console.log(err);
        })
    },[loadingMore])


    const onRefresh = async () => {
      setRefreshing(true);
      setLoadingMore(true)
      setPage(1)
    }

    const loadMore = () => {
      setPage(prevState=> prevState + 1)
      axios.get(`https://reqres.in/api/users?page=${page}`)
      .then((resp)=>{
        console.log(typeof (resp.data.data));
        if(resp.data.data !== {}){
        setData(data.concat(resp.data.data))
        }else{
          ToastAndroid.showWithGravityAndOffset(
            "There is no more data",
            ToastAndroid.LONG,
            ToastAndroid.CENTER,
            25,
            50
          );
        }
        setRefreshing(false)
      })
      .catch((err)=>{
      console.log(err);
      })
    }


  return(
    <View>
      <StatusBar hidden/>
        <Header style={{backgroundColor: '#F99C5E'}}>
          <Left>
            <Button transparent onPress={()=>navigation.goBack()} >
              <Image 
              source={require('../../components/assets/ic_back_white.png')} 
              style={{height: 13, width: 10}}/>
            </Button>
          </Left>
          <Body>
            <Title>GUESTS</Title>
          </Body>
        </Header>
       <FlatList
          data={data}
          renderItem={({item, index}) => (
            <Profile
              nama={item.first_name}
              image={item.avatar}
              id={item.id}
              key={index}
            />
          )}
          numColumns={2}
          bounces={false}
          keyExtractor={(item, index) => index.toString()}
          onEndReachedThreshold={0.1}
          onEndReached={loadMore}
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
          />
    </View>
  )
}
export default FourthScreen