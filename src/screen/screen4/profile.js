import React from 'react';
import { View, Text, Image, TouchableOpacity, ToastAndroid} from 'react-native';

const Profile=(props)=>{
    let Image_Http_URL ={ uri: `${props.image}`};

    React.useEffect(()=>{
        const number = parseInt(props.id);
        let isPrime = true;
        if (number === 1) {
            // console.log("1 is neither prime nor composite number.");
            ToastAndroid.showWithGravityAndOffset(
                "1 is neither prime nor composite number.",
                ToastAndroid.SHORT,
                ToastAndroid.TOP,
                25,
                50
              );
        }
        else if (number > 1) {
            for (let i = 2; i < number; i++) {
                if (number % i == 0) {
                    isPrime = false;
                    break;
                }
            }
        
            if (isPrime) {
                // console.log(`${number} is a prime number`);
                ToastAndroid.showWithGravityAndOffset(
                    `${number} is a prime number`,
                    ToastAndroid.SHORT,
                    ToastAndroid.TOP,
                    25,
                    50
                  );
            } else {
                // console.log(`${number} is a not prime number`);
                ToastAndroid.showWithGravityAndOffset(
                    `${number} is a not prime number`,
                    ToastAndroid.SHORT,
                    ToastAndroid.TOP,
                    25,
                    50
                  );
            }
        }
        else {
            // console.log("The number is not a prime number.");
            ToastAndroid.showWithGravityAndOffset(
                "The number is not a prime number.",
                ToastAndroid.SHORT,
                ToastAndroid.TOP,
                25,
                50
              );
        }
    })

return(
    <View style={{
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        marginHorizontal: '10%',
        marginVertical: '5%',
        marginBottom: '16%'
        }}>
        <TouchableOpacity>
        <Image 
        source={Image_Http_URL}
        style={{borderRadius: 90, height: 90, width: 90}}/>
        <Text style={{marginLeft: '25%'}}>
            {props.nama}
        </Text>
        </TouchableOpacity>
    </View>
)
}

export default Profile